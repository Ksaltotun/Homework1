"use strict";

const openArrRec = function(arr) {
  let userArr = arr;
  let buffMass = [];
  //buffMass = userArr;

  for (let i of userArr) {
    if (Array.isArray(i)) {
      for (let j of i) {
        buffMass.push(j);
      }
    } else {
      buffMass.push(i);
    }
  }
  for (let i of buffMass) {
    if (Array.isArray(i)) {
      return openArrRec(buffMass);
    }
  }
  return buffMass;
};

const calcSumm = function(array) {
  const calcSum = array;
  let summ = 0;
  for (let i in calcSum) {
    summ += calcSum[i];
  }
  document.querySelector("#result").innerHTML +=
    "You chose recursion! Summ= " + summ;
};
const whatTheButtonPresed = function() {
  let r = document.querySelector('input[name="choise"]:checked').value;
  if (r === "while") {
    treeSum(converseToArray());
  } else {
    calcSumm(openArrRec(converseToArray()));
  }
};
