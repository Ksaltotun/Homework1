"use strict";

const treeSum = function(arr) {
  let summa, bufMass, flag;
  summa = 0;
  bufMass = [];
  flag = false;
  let userArr = arr;
  let i = 0;

  while (!flag) {
    for (i = 0; i < userArr.length; i++) {
      if (!!userArr[i].length) {
        for (let j = 0; j < userArr[i].length; j++) {
          bufMass.push(userArr[i][j]);
        }
      } else {
        bufMass.push(userArr[i]);
      }
    }

    for (i = 0; i < bufMass.length; i++) {
      if (!Array.isArray(bufMass[i])) {
        flag = true;
      } else if (bufMass[i].length === 0) {
        bufMass[i] = 0;
      } else {
        flag = false;
        break;
      }
    }
    userArr = bufMass;
    bufMass = [];
  }

  for (i = 0; i < userArr.length; i++) {
    summa += userArr[i];
  }
  document.querySelector("#result").innerHTML +=
    "You chose while cycle! Summ= " + summa;
};

const converseToArray = function() {
  let inputString, castomArr;
  castomArr = [];
  inputString = document.querySelector("#inputBox").value;
  //document.querySelector("#result").innerHTML = 'result:';
  castomArr = eval(inputString);
  return castomArr;
};
